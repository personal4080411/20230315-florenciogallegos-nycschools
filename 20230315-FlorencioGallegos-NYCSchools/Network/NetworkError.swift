//
//  NetworkError.swift
//  20230315-FlorencioGallegos-NYCSchools
//
//  Created by Consultant on 3/15/23.
//

import Foundation

enum NetworkError: Error {
    
    case badURL
    case other(Error?)

}
