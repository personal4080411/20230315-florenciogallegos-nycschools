//
//  NetworkURL.swift
//  20230315-FlorencioGallegos-NYCSchools
//
//  Created by Consultant on 3/15/23.
//

import Foundation

struct NetworkURL {
    static let url = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let urlDetails = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
}
