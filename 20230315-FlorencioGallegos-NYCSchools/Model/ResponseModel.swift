//
//  ResponseModel.swift
//  20230315-FlorencioGallegos-NYCSchools
//
//  Created by Consultant on 3/15/23.
//

import Foundation

struct ResponseModel: Codable {

    let id: String?
    let schoolName: String?
    let reading: String?
    let math: String?
    let writing: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case schoolName = "school_name"
        case reading = "sat_critical_reading_avg_score"
        case math = "sat_math_avg_score"
        case writing = "sat_writing_avg_score"
    }
}
